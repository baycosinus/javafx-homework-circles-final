import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import sun.security.ssl.Debug;

public class Main extends Application
{
    //Define Panes
    public BorderPane bPane = new BorderPane(); // Main Frame
    public BorderPane oPane = new BorderPane(); //Orbit Frame
    public HBox ui = new HBox(); // UI Frame
    //Define Variables
    public static int nodeCount;
    public static String nodeHash;
    public static int orbitRadius;
    public static int windowSize = 1000;
   
   
    public void InitializeGUI() // Some makeup could be fine.
    {
        
       //Define Labels
        Label nodeCountLabel = new Label("Node Count: ");
        Label nodeHashLabel = new Label("Node Hash: ");
        Label orbitRadiusLabel = new Label("Orbit Radius: ");
        
       //Define TextFields
        final TextField nodeCountField = new TextField();
        final TextField nodeHashField = new TextField();
        final TextField orbitRadiusField = new TextField();
        
       //Define Buttons
        Button generateButton = new Button("Generate");
        generateButton.setOnAction(new EventHandler<ActionEvent>()
                {
                @Override
                public void handle(ActionEvent event)
                {
                    nodeCount = Integer.parseInt(nodeCountField.getText());
                    nodeHash = nodeHashField.getText();
                    orbitRadius = Integer.parseInt(orbitRadiusField.getText());
                    if(nodeHash.length() == nodeCount){
                    draw();
                    }
                }
                });
        
        ui.getChildren().addAll(nodeCountLabel,nodeCountField,nodeHashLabel,nodeHashField,orbitRadiusLabel,orbitRadiusField,generateButton);
        
    }
    @Override
    public void start (Stage primaryStage)
    {
        InitializeGUI();
        
        bPane.setTop(ui);
        bPane.setCenter(oPane);
        Stage window = primaryStage;
        Scene scene = new Scene (bPane, windowSize,windowSize);
        window.setScene(scene);
        window.show();
    }
    public static void main(String[] args)
    {
        launch(args);
    }
     public void draw() // One function to rule them all.
    {
        Circle orbit = new Circle(oPane.getWidth()/2,oPane.getHeight()/2,orbitRadius);
        orbit.setStroke(Color.BLACK);
        orbit.setFill(Color.TRANSPARENT); // Invisibility is bliss.
        oPane.getChildren().add(orbit);
        String[] splitNodeHash = nodeHash.split("(?!^)"); // Chop them up, leave none alive.
        for(int i = 0; i < nodeCount; i++)
        {
            double x = orbitRadius * Math.cos((360/nodeCount) * i/(180/Math.PI)) + (oPane.getWidth()/2);
            double y = orbitRadius * Math.sin((360/nodeCount) * i/(180/Math.PI)) + (oPane.getHeight()/2);
            System.out.println(x);
            Circle node = new Circle(x,y,20);
            node.setStroke(Color.BLACK); // Hello darkness, my old friend.
            if (Integer.parseInt(splitNodeHash[i]) == 1){
            node.setFill(Color.BLUE); // Or red..? Make a choice, Neo. It'll change your life.
            }
            else if (Integer.parseInt(splitNodeHash[i]) == 0)
            {
            node.setFill(Color.RED);
            }
            oPane.getChildren().add(node);
        }
    }
     
}